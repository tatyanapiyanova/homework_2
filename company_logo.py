# https://www.hackerrank.com/challenges/most-commons/problem
# !/bin/python

import math
import os
import random
import re
import sys
from collections import Counter
from collections import OrderedDict
if __name__ == '__main__':
    logo = sorted(raw_input())

    symbol_max = Counter(sorted(logo)).most_common(3)
    sort=sorted(sorted(symbol_max, key = lambda x: x[0]), key = lambda x: x[1], reverse = True)

    for k,v in sort:
        print "{} {}".format(k, v)