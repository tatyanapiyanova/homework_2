"""Scraper for https://secure.flydanaair.com/"""

import bookingengine

URL = 'https://secure.flydanaair.com/'


DANAAIR = bookingengine.Scraper(URL)
FLY_FROM, FLY_TO, DATE_D, DATE_R = DANAAIR.data_input()
DANAAIR.load_booking(FLY_FROM, FLY_TO, DATE_D, DATE_R)
#DANAAIR.load_booking('ABV', 'PHC', '30/08/2019', '01/09/2019')
#DANAAIR.load_booking('ABV', 'PHC', '30/08/2019')
