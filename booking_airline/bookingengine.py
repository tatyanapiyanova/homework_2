"""
Scraper
"""
from __future__ import print_function
from datetime import datetime
import requests
from lxml import html


class Scraper(object):
    """
    Engine
    """

    def __init__(self, url):
        self.url = url
        self.sess = requests.Session()

    def load_booking(self, depart_place, return_place, depart_date, return_date):
        """Data retrieval"""
        query = self.get_param(depart_place, return_place, depart_date, return_date)
        tree = self.get_html(query)
        if return_date is None:
            table = self.get_block_parse(1, depart_date)
            print('{0}-{1}'.format(depart_place, return_place))
            try:
                self.parse(tree, table)
            except IndexError:
                print("No flights found!!!")
        else:
            table_1 = self.get_block_parse(1, depart_date)
            table_2 = self.get_block_parse(2, return_date)
            print('{0}-{1}'.format(depart_place, return_place))
            try:
                self.parse(tree, table_1)
            except IndexError:
                print("No flights found!!!")
            print('{0}-{1}'.format(return_place, depart_place))
            try:
                self.parse(tree, table_2)
            except IndexError:
                print("No flights found!!!")

    @staticmethod
    def get_block_parse(index, date_):
        """Get block from html for parsing"""
        table = '//table[@id = "trip_{0}_date_{1}_{2}_{3}"]'.format(
            index,
            date_.strftime('%Y'),
            date_.strftime('%m'),
            date_.strftime('%d'))
        return table

    def get_param(self, depart_place, return_place, depart_date, return_date=None):
        """Parameters for the http-request."""
        if return_date is None:  # one way
            url_query = self.get_query(depart_place, return_place, depart_date)
        else:  # round trip
            url_query_tmp = self.get_query(depart_place, return_place, depart_date, 'RT')
            date_rm = return_date.strftime('%Y-%m')
            date_rd = return_date.strftime('%d')
            url_query = '{0}&RM={1}&RD={2}'.format(url_query_tmp, date_rm, date_rd)
        return url_query

    def get_query(self, depart_place, return_place, depart_date, param_tt='OW'):
        """Parameters for the http-request"""
        date_am = depart_date.strftime('%Y-%m')
        date_ad = depart_date.strftime('%d')
        url_query = '{0}bookings/flight_selection.aspx?' \
                    'TT={1}&FL=on&DC={2}&AC={3}&AM={4}&AD={5}&PA=1'.format(
                        self.url, param_tt, depart_place, return_place, date_am, date_ad)
        return url_query

    def get_html(self, url_query):
        """Get html text"""
        request = self.sess.get(url_query)
        text = request.text
        tree = html.fromstring(text)
        return tree

    @staticmethod
    def format_date(time_):
        """Change date format"""
        try:
            time_ = datetime.strptime(time_, "%H:%M")
        except ValueError:
            time_ = datetime.strptime(time_, "%I:%M %p")
        return time_

    def parse(self, tree, table):
        """Parsing html"""
        body = tree.xpath(table)[0].xpath('.//tbody')
        for item_body in body:
            flight = item_body.xpath('.//td[@class = "flight"]')
            if not flight:
                print('No flights found!!!')
            else:
                rate = []
                for item_flight in flight:
                    time_leaving_back = self.format_date(item_flight.xpath(
                        '..//td[@class = "time leaving"]/text()')[0])
                    time_landing_back = self.format_date(item_flight.xpath(
                        '..//td[@class = "time landing"]/text()')[0])
                    flight_duration = time_landing_back - time_leaving_back
                    seconds = flight_duration.total_seconds()
                    hours = abs(flight_duration.days*24) + seconds//3600
                    minutes = (seconds % 3600)//60
                    rate1 = item_flight.xpath('..//label/@data-title')
                    rate1 = [item.splitlines()[1] for item in rate1]
                    rate.extend(rate1)
                    rate_sold = item_flight.xpath('..//label[@class = "no_fare"]/text()')
                    rate_sold = [item for item in rate_sold]
                    rate.extend(rate_sold)
                    path = item_flight.xpath('..//td[@class = "route"]//span/text()')
                    if 'Nonstop' in path:
                        print(path[0])
                    else:
                        print('{0} - {1}'.format(path[0], path[1]))
                    print('time_leaving: {:%H:%M}'.format(time_leaving_back))
                    print('time_landing: {:%H:%M}'.format(time_landing_back))
                    print('flight_duration: {:g} hours {:g} minutes'.format(hours, minutes))
                print('rate: {0}'.format(rate))

    def city_input(self):
        """Checking for the correct city entry"""
        tree = self.get_html(self.url)
        block = tree.xpath('//select[@name = "DC"]/option/@value')
        block_city = [i for i in block if i]
        fly_city = None
        while fly_city is None:
            input_value = input()
            input_value = input_value.upper()
            if input_value not in block_city or input_value is None:
                print('You have entered the wrong city code! Enter the code from the list {0}!'
                      .format(set(block_city)))
            else:
                fly_city = input_value
        return fly_city

    def data_input(self):
        """Input parameters"""
        print("Flying from:")
        fly_from = self.city_input()
        print("Flying to:")
        fly_to = self.city_input()
        print('Departure date (d/m/yyyy):')
        date_d = self.date_depart_input()
        print("Return date (d/m/yyyy):")
        date_r = self.date_return_input()
        return fly_from, fly_to, date_d, date_r

    @staticmethod
    def date_depart_input():
        """Checking for correct entry date of departure"""
        date_d = None
        while date_d is None:
            input_value = input()
            try:
                date_d = datetime.strptime(input_value, '%d/%m/%Y')
            except ValueError:
                print('Invalid date! Enter date in format "d/m/yyyy"!')
        return date_d

    @staticmethod
    def date_return_input():
        """Checking for correct entry date of return"""
        date_r = None
        while date_r is None:
            input_value = input()
            if input_value == '':
                date_r = None
                break
            else:
                try:
                    date_r = datetime.strptime(input_value, '%d/%m/%Y')
                except ValueError:
                    print('Invalid date!Try again!')
        return date_r
