"""Scraper for https://www.biman-airlines.com/"""
import bookingengine

class Biman(bookingengine.Scraper):
    """Scraper for https://www.biman-airlines.com/"""

    def get_fs(self, depart_place, return_place, depart_date):
        """Additional parameter for http-request"""
        date_am = depart_date.strftime('%Y-%m')
        date_ad = depart_date.strftime('%d')
        data_1 = {'X-Hash-Validate': 'TT=RT&FL=on&DC={0}&AC={1}&AM={2}&AD={3}&PA=1'.format(
            depart_place, return_place, date_am, date_ad)}
        resp = self.sess.head('https://www.biman-airlines.com/bookings/captcha.aspx',
                              headers=data_1)
        fs_param = resp.headers['X-Hash']
        return fs_param

    def get_param(self, depart_place, return_place, depart_date, return_date=None):
        """Parameters for the http-request"""
        fs_param = self.get_fs(depart_place, return_place, depart_date)
        url_query_tmp = super(Biman, self).get_param(depart_place, return_place,
                                                     depart_date, return_date)
        url_query = '{0}&FS={1}'.format(url_query_tmp, fs_param)
        return url_query

url = 'https://www.biman-airlines.com/'
BIMAN = Biman(url)
FLY_FROM, FLY_TO, DATE_D, DATE_R = BIMAN.data_input()
BIMAN.load_booking(FLY_FROM, FLY_TO, DATE_D, DATE_R)

#biman.load_booking('AUH', 'CGP', '03/09/2019')
#BIMAN.load_booking('DAC', 'SPD', '29/08/2019', '30/08/2019')
