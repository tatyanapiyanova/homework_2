# https://www.hackerrank.com/challenges/python-time-delta/problem
# !/bin/python

import math
import os
import random
import re
import sys
from datetime import datetime, timedelta


# Complete the time_delta function below.
def time_delta(t1, t2):
    new_t1 = datetime.strptime(t1[0:24], '%a %d %b %Y %H:%M:%S')
    if t1[25] == '+':
        new_t1 -= timedelta(hours=int(t1[26:28]), minutes=int(t1[28:]))
    elif t1[25] == '-':
        new_t1 += timedelta(hours=int(t1[26:28]), minutes=int(t1[28:]))

    new_t2 = datetime.strptime(t2[0:24], '%a %d %b %Y %H:%M:%S')
    if t2[25] == '+':
        new_t2 -= timedelta(hours=int(t2[26:28]), minutes=int(t2[28:]))
    elif t2[25] == '-':
        new_t2 += timedelta(hours=int(t2[26:28]), minutes=int(t2[28:]))

    time_delta = new_t1 - new_t2

    return str(abs(time_delta.days * 24 * 3600 + time_delta.seconds))


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(raw_input())

    for t_itr in xrange(t):
        t1 = raw_input()

        t2 = raw_input()

        delta = time_delta(t1, t2)