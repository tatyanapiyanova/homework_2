#https://www.hackerrank.com/challenges/merge-the-tools/problem
def merge_the_tools(string, k):
    i=0
    while i<len(string):
        substr = []
        for s in string[i:i+k]:
            if s not in substr:
                substr.append(s)
        print(''.join(substr))
        i = i + k