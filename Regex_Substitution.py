# https://www.hackerrank.com/challenges/re-sub-regex-substitution/problem
import re
n=int(raw_input())

for i in range(n):
    text=raw_input()
    new_text=re.sub(r'(?<=\s)(&&|[|]{2})(?=\s)',lambda x: 'and' if x.group() == '&&' else 'or',text)
    print new_text
