# https://www.hackerrank.com/challenges/no-idea/problem
n, m = map(int, raw_input().split())
n_m = raw_input().split(' ')
A = set(raw_input().split(' '))
B = set(raw_input().split(' '))
happiness = 0
for i in n_m:
    if i in A:
        happiness += 1
    if i in B:
        happiness -= 1

print happiness