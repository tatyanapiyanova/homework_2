# https://www.hackerrank.com/challenges/validating-credit-card-number/problem
import re
n=int(raw_input())

for i in range(n):
    card=raw_input()
    if re.match(r'^[456][0-9]',card) and re.match(r'(\d{16}$)|(\d{4}-){3}\d{4}$',card) and not re.search(r'(\d)\1{3,}', re.sub(r'-', "", card)):
       print "Valid"
    else:
       print "Invalid"