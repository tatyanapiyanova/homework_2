# https://www.hackerrank.com/challenges/iterables-and-iterators/problem
from itertools import combinations
col=int(raw_input())
l=list(map(str, raw_input().split()))
index=int(raw_input())
lst_comb=list(combinations(l,index))
s=0
for i in lst_comb:
    if 'a' in i:
       s+=1

print '%.3f' % (float(s)/len(lst_comb))