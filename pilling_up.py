# https://www.hackerrank.com/challenges/piling-up/problem
for n in range(int(raw_input())):
    k = raw_input()
    #print "numbers count: %s" % k
    cube = map(int, raw_input().split())
    #print "cubes: %s" % cube
    if cube[0] >= cube[-1]:
        top = cube[0]
        del cube[0]
    elif cube[0] <= cube[-1]:
        top = cube[-1]
        del cube[-1]
    while len(cube) > 0:
        if cube[0] >= cube[-1] and cube[0]<=top :
            top=cube[0]
            del cube[0]
        elif cube[0] <= cube[-1] and cube[-1]<=top:
            top=cube[-1]
            del cube[-1]
        else:
            break

    if len(cube) == 0:
      print('Yes')
    else:
      print ('No')