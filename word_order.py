#https://www.hackerrank.com/challenges/word-order/problem
from collections import OrderedDict
dict_word=OrderedDict()
for i in range(int(raw_input())):
 word = raw_input()
 if word not in dict_word:
  dict_word[word]=1
 else:
  dict_word[word] += 1
print len(dict_word)
print ' '.join(str(k) for k in dict_word.values())
