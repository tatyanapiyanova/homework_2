"""
The module analyzes information on airports, stations and terminals.
"""
import csv
import re
import requests

def csv_writer(url, path):
    """
    Request through the requests library to the URL.
    Data output in the form
    | Name | City | IATA | ICAO | Type |
    The output file is "airport.csv"
    """
    lst_line = []
    dat_file = requests.get(url)
    text = dat_file.text.splitlines()
    for line in text:
        if line.find('""'):
            line = line.replace('""', ' ')
        line = re.findall(r'(".+?"|[^,]+)', line)
        if 'unknown' in line[-2] or '\\N' in line[-2]:
            continue
        else:
            lst_tmp = [line[1], line[2], line[4], line[5], line[-2]]
            for n,i in enumerate(lst_tmp):
                if i == '\\N':
                    lst_tmp[n] = 'None'
            lst_line.append(lst_tmp)
    with open(path, "w", encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE, delimiter='|', quotechar='')
        for i in lst_line:
            writer.writerow(i)

def reader_dict(url):
    """
    Creating a dictionary, keys are the names of airports (only airports need to be taken),
    and values are coordinates.
    """
    air_dict = {}
    dat_file = requests.get(url)
    text = dat_file.text.splitlines()
    for line in text:
        if line.find('""'):
            line = line.replace('""', ' ')
        line = re.findall(r'(".+?"|[^,]+)', line)
        if 'airport' in line[-2]:
            air_dict.update({line[1]: (line[6], line[7])})
    print(air_dict)

def equator(url):
    """
    A function that lists airports in order of their distance from the equator.
    """
    air_equator = {}
    dat_file = requests.get(url)
    text = dat_file.text.splitlines()
    for line in text:
        if line.find('""') and line.find('airport'):
            line = line.replace('""', ' ')
        line = re.findall(r'(".+?"|[^,]+)', line)
        if 'airport' in line[-2]:
            distance_equator = abs(float(line[6]))*111
            air_equator.update({line[1]: distance_equator})

    sorted_air = sorted(air_equator.items(), key=lambda distance: distance[1])
    print(sorted_air[:51])

if __name__ == "__main__":
    URL = 'https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports-extended.dat'
    PATH = 'airport.csv'
    csv_writer(URL,PATH)
    reader_dict(URL)
    equator(URL)