"""Own implementation of the function zip()"""
def my_zip(*args):
    """
    Returns an iterator of tuples, where the i-th tuple contains the i-th element
    from each of the argument sequences or iterables.
    The iterator stops when the shortest input iterable is exhausted.
    """
    l_len = []
    for i in args:
        l_len.append(len(i))
    min_len = min(l_len)

    my_list = []
    for k in xrange(min_len):
        tmp_list = []
        for j in xrange(min_len):
            tmp_list.append(args[j][k])
        my_list.append(tuple(tmp_list))
    return my_list

print my_zip([1, 2, 3, 4, 5], 'abcde', (1, 2, 3))
