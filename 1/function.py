"""Functions where single argument - a list of numbers."""
def square_of_number(numbers):
    """A function that returns a list of squares of numbers."""
    square = [number**2 for number in numbers]
    #square=map(lambda x: x**2,numbers)
    return square

print square_of_number([1, 2, 3])

def two_element(numbers):
    """A function that returns every second list item."""
    return numbers[::2]

print two_element([1, 2, 3, 4, 5, 6, 7, 8, 9])

def square_even(numbers):
    """A function that returns the squares of even elements at odd positions."""
    square = [numbers[number]**2 for number in range(len(numbers))
              if not number%2 == 0 and numbers[number]%2 == 0]
    return square

print square_even([2, 3, 3, 4, 5, 6, 7, 8, 9])
