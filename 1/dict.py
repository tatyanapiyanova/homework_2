# -*- coding: utf-8 -*-
"""
Script create function that accepts a dictionary as input,
and returns a dictionary in which keys with values ​​​​change.
"""
def dict_reverse(dict_kv):
    """
    A function that returns a dictionary in which keys with values ​​change.
    """
    try:
        dict_rev = {key_: value_ for key_ in dict_kv.values() for value_ in dict_kv.keys()}
    except TypeError:
        print 'Invalid key value!'
    else:
        return dict_rev

DICT_TEST = {1: 'home', 2: ('flat', 'flat_1')}
print dict_reverse(DICT_TEST)
DICT_TEST = {1: 'home', 2: ['flat', 'flat_1']}
print dict_reverse(DICT_TEST)
