"""Own implementation of the function xrange()"""
def my_xrange(number):
    """
    This function returns the generator object
    that can be used to display numbers only by looping.
    """
    count = 0
    while count < number:
        yield count
        count += 1

MY_GENERATOR = my_xrange(6)  #Create object generator
for count_g in MY_GENERATOR:
    print count_g
