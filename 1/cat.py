"""Script function cat without limiting the number of argument files"""
def my_cat(*args):
    """Function cat without limiting the number of argument files"""
    for filename in args:
        file_ = open(filename)
        for line in file_:
            print line
    file_.close()

print my_cat('C:/Users/user/Documents/Phyton/Homework_2/1/dict.py',
             'C:/Users/user/Documents/Phyton/Homework_2/1/mygen.py',
             'C:/Users/user/Documents/Phyton/Homework_2/1/reduce.py')
