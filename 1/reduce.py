"""
https://www.hackerrank.com/challenges/reduce-function/problem
"""
from __future__ import print_function
from fractions import Fraction

def product(fracs):
    """Given a list of rational numbers,find their product."""
    number = reduce(lambda x, y: x*y, fracs)
    return number.numerator, number.denominator

if __name__ == '__main__':
    FRACS = []
    for _ in range(input()):
        FRACS.append(Fraction(*map(int, raw_input().split())))
    RESULT = product(FRACS)
    print(*RESULT)
