# -*- coding:utf-8 -*-
"""Scraper"""

import requests
from bs4 import BeautifulSoup

def load_user_data(url, filename):
    """load web page"""

    cookie = {'PHPSESSID': '1fvnca18fug2c45uc5rnv85hm3',
              '_ym_uid': '1565693782752814274',
              '_ym_d': '1565693782',
              '_ga': 'GA1.2.1885679797.1565693782',
              '_gid': 'GA1.2.1938462785.1565693782', '_ym_isad': '2',
              'lang': 'yes', '_ym_visorc_57699': 'w'}

    data = {'num': 5,
            'val1': 1,
            'val2': 0,
            'options': '0;3083@0;68@0;68@@@@@@@@@4@@',
            'filter': 'count:4'}
    request = requests.post(url, data=data, cookies=cookie)
    print 'loading page %s' % data['val1']
    text = request.text.encode('cp1251')

    soup = BeautifulSoup(text, "html.parser")
    page = soup.find_all('span', "page")[-1]
    page_end = int(page.find('a').text) # number end page
    parse_web(text, filename)

    for page in range(2, page_end+1):
        data = {'num': 5,
                'val1': page,
                'val2': 0,
                'options': '0;3083@0;68@0;68@@@@@@@@@4@@',
                'filter': 'count:4'}
        request = requests.post(url, data=data, cookies=cookie)
        text = request.text.encode('cp1251')
        print 'loading page %s' % data['val1']
        parse_web(text, filename)


def parse_web(text, filename):
    """
    Parsing web page
    """

    soup = BeautifulSoup(text, "html.parser")
    items = soup.find_all('div', {'class': 'good-description'})
    index = 0
    for item in items:
        art = item.find('span', 'good-name').text
        price = item.find('span', 'price').text
        index += 1
        with open(filename, 'a') as out_file:
            out_file.write('{0}, {1}\n'.format(art.encode('cp1251'), \
                                               price.encode('cp1251')))

    print index


if __name__ == "__main__":
    FILENAME = 'test.txt'
    URL = 'https://panna.ru/ajax/catalog.php'
    load_user_data(URL, FILENAME)
