"""
https://www.hackerrank.com/challenges/validate-list-of-email-address-with-filter/problem
"""
import re
def valid_email(email_):
    """Returns a list containing only valid email addresses in lexicographical order."""
    if re.match(r"^[A-Za-z0-9-_]+@[A-Za-z0-9]+\.[A-Za-z]{1,3}$", email_):
        return True
    return False

def filter_mail(emails):
    """Returns an iterator containing only valid email addresses."""
    return filter(valid_email, emails)

if __name__ == '__main__':
    COUNT = int(raw_input())
    EMAILS = []
    for _ in range(COUNT):
        EMAILS.append(raw_input())
    FILTERED_EMAILS = filter_mail(EMAILS)
    FILTERED_EMAILS.sort()
    print FILTERED_EMAILS
