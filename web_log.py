"""
log file analysis
"""
import re
from collections import Counter

def read_file(filename):
    """open file"""
    log_file = open(filename)
    return log_file

def client(log):
    """
    10 clients of this server who have requested the largest number of pages
    in descending order of the number of requested pages
    """
    ip_list = []
    for line in read_file(log):
        str_find = re.search(r'(\s)-(\s)-(\s)', line)
        if str_find is None:
            continue
        ip_client = line[:str_find.start()]
        ip_list.append(ip_client)
    ip_dict = Counter(ip_list)
    for i in ip_dict.most_common(10):
        print '%s: %d' % (i[0], i[1])

def platform(log):
    """platforms to launch web browsers in descending order of popularity"""
    platform_list = []
    for line in read_file(log):
        str_ = re.findall(r'(?<=\().+?(?=\))', line)
        rez = ''.join(str_)
        if rez == "":
            continue
        if re.search(r'(\w*Windows\w*)', rez) is not None:
            platform_list.append("Windows")
        elif re.search(r'(\w*Linux\w*)', rez) is not None:
            platform_list.append("Linux")
        elif re.search(r'(\w*http\w*)', rez) is not None:
            platform_list.append("bot")
        elif re.search(r'(\w*Mac\sOS\w*)', rez) is not None:
            platform_list.append("Macintosh")
        else:
            platform_list.append("Other")
    platform_dict = Counter(platform_list)
    for i in platform_dict.items():
        print '%s: %d' % (i[0], i[1])


if __name__ == "__main__":

    client('C:/Users/user/Documents/Phyton/access2.log')
    print '*'*100
    platform('C:/Users/user/Documents/Phyton/access2.log')
